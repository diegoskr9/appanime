package com.example.applicationanime.model

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.applicationanime.R

class AnimaAdapter(var itemsAnimes:ArrayList<Anime>) : RecyclerView.Adapter<AnimaAdapter.ViewHolder>() {


    class ViewHolder(view:View):RecyclerView.ViewHolder(view){

            fun bindItem(anime:Anime){

                var title : TextView = itemView.findViewById(R.id.textViewAnime)
                var image : ImageView = itemView.findViewById(R.id.imageViewAnime)

                Glide.with(itemView.context).load(anime.image_url).into(image)
                title.text = anime.title

                itemView.setOnClickListener{

                    Toast.makeText(itemView.context, anime.title, Toast.LENGTH_LONG).show()

                }


            }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        var vista = LayoutInflater.from(parent.context).inflate(R.layout.anime_content,parent,false)

        return ViewHolder(vista)

    }

    override fun getItemCount(): Int {
       return itemsAnimes.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.bindItem(itemsAnimes[position])
    }


}