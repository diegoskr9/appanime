package com.example.applicationanime

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.GridView
import android.widget.LinearLayout
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import com.example.applicationanime.model.AnimaAdapter
import com.example.applicationanime.model.Anime
import com.example.applicationanime.model.ResultsAnime
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.anime_content.*
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btnSearch.setOnClickListener(View.OnClickListener {

            if(editTextAnime.text.isNullOrEmpty()){
                Toast.makeText(this,"Debe ingresar un anime",Toast.LENGTH_SHORT).show()
            }else{
                getDataAnime(editTextAnime.text.toString())
            }

        })


    }

    private fun getDataAnime(filter:String){

        var queue = Volley.newRequestQueue(this)

        var url = "https://api.jikan.moe/v3/search/anime?q=${filter}"

        val request = JsonObjectRequest(Request.Method.GET,url,null, Response.Listener { response ->

            try {

                var anime = Gson().fromJson(response.toString(),ResultsAnime::class.java)

                createRecyclerAnime(anime.results)


            }catch (e: Exception){

            }
        }, Response.ErrorListener { error ->

            Toast.makeText(this,error.toString(), Toast.LENGTH_SHORT).show()
        })
        queue.add(request)


    }

    @SuppressLint("WrongConstant")
    fun createRecyclerAnime(results: ArrayList<Anime>) {

        //myRecyclerAnime.layoutManager = LinearLayoutManager(this, LinearLayout.VERTICAL,false)

        myRecyclerAnime.layoutManager = GridLayoutManager(this,2)

        var myAdapter = AnimaAdapter(results)

        myRecyclerAnime.adapter = myAdapter


    }


}
